package edu.sjsu.android.mortgagecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    double iRate;
    double monthlyPayment = 0;

    RadioGroup loanTerm;
    EditText borrowedAmount;
    SeekBar interestRate;
    TextView payment;
    TextView rateLabel;
    CheckBox tax;
    CheckBox insurance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        borrowedAmount = (EditText) findViewById(R.id.borrowedAmount);
        payment = (TextView) findViewById(R.id.payment);
        interestRate = (SeekBar) findViewById(R.id.interestRate);
        rateLabel = (TextView) findViewById(R.id.rateLabel);
        loanTerm = (RadioGroup) findViewById(R.id.loanTerm);//need to check how the value is returned and stored.
        tax = (CheckBox) findViewById(R.id.tax);
        insurance = (CheckBox) findViewById(R.id.insurance);


        interestRate.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            iRate = interestRate.getProgress() / 10.0;
                            rateLabel.setText("Interest Rate is: " + iRate + " % ");
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        //called when touched of seekbar

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        //Called when untouched of seekbar
                    }
                }
        );
    }

    public void calculate(View v) {

        monthlyPayment = 0;
        int selectedTerm = loanTerm.getCheckedRadioButtonId();
        RadioButton selected = (RadioButton) findViewById(selectedTerm);
        String str = borrowedAmount.getText().toString();
        if (str.matches(".*[a-z].*")) {
            payment.setText("Please enter numbers");

        } else {
            float temp1 = Float.parseFloat(str);
            if (temp1 > 0.00 && selectedTerm != -1) {


                payment.setText("id: " + getLoan(selected.getText() + ""));
                double N = getLoan(selected.getText() + "") * 1.00 * 12;
                double P = Double.parseDouble(borrowedAmount.getText() + "");
                double T = P * 0.001;

                if (iRate == 0) {
                    monthlyPayment = (P / N);

                    if(tax.isChecked() || insurance.isChecked())
                        monthlyPayment += T;

                    String result = String.format("%1$.2f/month", monthlyPayment);
                    payment.setTextColor(Color.BLACK);
                    payment.setText("Payment: " + result);
                } else {

                    double temp = iRate / 1200.00;

                    double denominator = 1.00 - Math.pow((1.00 + temp), -N);
                    double numerator = temp;
                    monthlyPayment = P * (numerator / denominator);

                    if(tax.isChecked() || insurance.isChecked())
                        monthlyPayment += T;

                    String result = String.format("%1$.2f/month", monthlyPayment);
                    payment.setTextColor(Color.BLACK);
                    payment.setText("Payment: " + result);

                }
            }
             else{

                    payment.setText("wrong input");
                    payment.setTextColor(Color.RED);
                }
            }
        }

    public int getLoan(String text) {
        int result = 0;
        String[] temp = text.split(" ");
        result = Integer.parseInt(temp[0]);
        return result;
    }
}